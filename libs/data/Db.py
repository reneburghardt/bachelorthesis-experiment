import pymongo
from pymongo import MongoClient
import os

class Db:
		
	def __init__(self):
		client = MongoClient(os.environ['MONGO_URL'])
		self.db = client[os.environ['MONGO_DB']]
		self.db.authenticate(os.environ['MONGO_USER'], os.environ['MONGO_PASSWORD'])
		
	def getDb(self):
		return self.db