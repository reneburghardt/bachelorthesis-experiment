from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import LinearRegression
from sklearn.utils.validation import check_is_fitted
import numpy as np

class ModelTree(DecisionTreeRegressor):
	def __init__(self,
				 criterion="mse",
				 splitter="best",
				 max_depth=None,
				 min_samples_split=2,
				 min_samples_leaf=1,
				 min_weight_fraction_leaf=0.,
				 max_features=None,
				 random_state=None,
				 max_leaf_nodes=None,
				 min_impurity_decrease=0.,
				 min_impurity_split=None,
				 presort=False):
		super(ModelTree, self).__init__(
			criterion=criterion,
			splitter=splitter,
			max_depth=max_depth,
			min_samples_split=min_samples_split,
			min_samples_leaf=min_samples_leaf,
			min_weight_fraction_leaf=min_weight_fraction_leaf,
			max_features=max_features,
			max_leaf_nodes=max_leaf_nodes,
			random_state=random_state,
			min_impurity_decrease=min_impurity_decrease,
			min_impurity_split=min_impurity_split,
			presort=presort)
			
	def fit(self, X, y, sample_weight=None, check_input=True, X_idx_sorted=None):
		super(ModelTree, self).fit(X, y, sample_weight, check_input, X_idx_sorted)
		
		self.leaf_models = [None for i in range(self.tree_.node_count)]	# new set for linreg-models of leafs		
		leafsOfX = self.tree_.apply(np.array(X).astype(np.float32)) # get leaf for each entry in X
		samplesOfLeafs_X = [[] for i in range(self.tree_.node_count)] # new sets to collect samples for each leaf
		samplesOfLeafs_y = [[] for i in range(self.tree_.node_count)]
		for i in range(len(X)):
			samplesOfLeafs_X[leafsOfX[i]].append(X[i]) # add each entry to the collection of its leaf (per node id of leaf)
			samplesOfLeafs_y[leafsOfX[i]].append(y[i])
			
		for i in range(len(samplesOfLeafs_X)):
			X = samplesOfLeafs_X[i] # for each leaf, get sample set
			y = samplesOfLeafs_y[i]
			if len(X) > 0:
				self.leaf_models[i] = LinearRegression().fit(X, y) # if sample contains at least one entry, train linreg
		
		return self
		
	def predict(self, X, check_input=True):
		check_is_fitted(self, 'tree_') # check whether tree is already trained
		X = self._validate_X_predict(X, check_input) # validate X
		leafsOfX = self.tree_.apply(X) # get leaf of X
		predictions = []
		for i in range(len(leafsOfX)): 
			predictions.append(
				self.leaf_models[leafsOfX[i]].predict([X[i]])[0] # collect prediction of linreg-model for each entry of X
			)
			
		return np.ravel(np.array(predictions)) # some array handling for compatibility