from numpy import abs, median, delete
from random import randint

class Metrics:

	def all(self, actual, predicted):
		return {
			'mmre': self.mmre(actual, predicted),
			'mdmre': self.mdmre(actual, predicted),
			'mmer': self.mmer(actual, predicted),
			'mdmer': self.mdmer(actual, predicted),
			'pred': self.pred(actual, predicted),
			'mae': self.mae(actual, predicted),
			'mbre': self.mbre(actual, predicted),
			'mibre': self.mibre(actual, predicted),
			'sa': self.sa(actual, predicted)
		}
		
	def mre(self, actual, predicted):
		mres = []
		for i in range(len(predicted)):
			mres.append(abs((actual[i] - predicted[i]) / actual[i]))
		return mres
		
	def mmre(self, actual, predicted):
		mres = self.mre(actual, predicted)
		mmre = 0
		for mre in mres:
			mmre += mre
			
		mmre /= len(mres)
		return mmre
		
	def mdmre(self, actual, predicted):
		mres = self.mre(actual, predicted)
		mdmre = median(mres)
		return mdmre
		
	def pred(self, actual, predicted, x=0.25):
		mres = self.mre(actual, predicted)
		k = 0
		for mre in mres:
			k = k + 1 if mre <= x else k
			
		pred = k/len(mres)
		return pred
		
	def mer(self, actual, predicted):
		mers = []
		for i in range(len(predicted)):
			mers.append(abs((actual[i] - predicted[i]) / predicted[i]))
		return mers
		
	def mmer(self, actual, predicted):
		mers = self.mer(actual, predicted)
		mmer = 0
		for mer in mers:
			mmer += mer
			
		mmer /= len(mers)
		return mmer
		
	def mdmer(self, actual, predicted):
		mers = self.mer(actual, predicted)
		mdmer = median(mers)
		return mdmer
		
	def ae(self, actual, predicted):
		aes = []
		for i in range(len(predicted)):
			aes.append((abs(actual[i] - predicted[i])))
		return aes
		
	def mae(self, actual, predicted):
		aes = self.ae(actual, predicted)
		mae = 0
		for ae in aes:
			mae += ae
			
		mae /= len(aes)
		return mae
		
	def mbre(self, actual, predicted):
		mbre = 0
		for i in range(len(predicted)):
			min = actual[i] if actual[i] <= predicted[i] else predicted[i]
			mbre += abs((actual[i] - predicted[i])/min)
		
		mbre /= len(predicted)
		return mbre
	
	def mibre(self, actual, predicted):
		mibre = 0
		for i in range(len(predicted)):
			max = actual[i] if actual[i] >= predicted[i] else predicted[i]
			mibre += abs((actual[i] - predicted[i])/max)
		
		mibre /= len(predicted)
		return mibre
	
	def lsd(self, actual, predicted):
		pass
	
	def sa(self, actual, predicted):
		sa = 1
		mae = self.mae(actual, predicted)
		guessed = self.random_prediction(actual)
		mae_random = self.mae(actual, guessed)
		sa -= (mae/mae_random)
		
		return sa
		
	def random_prediction(self, actual):
		# TODO: len(actual) == 1
		random_predicted = []
		for i in range(len(actual)):
			sub = delete(actual, i)
			r = randint(0, len(sub)-1)
			random_predicted.append(sub[r])
			
		return random_predicted