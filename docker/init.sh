#!/bin/bash

cd /opt/app-root/src/mongodb-init/csv;

for filename in ./*.csv; do
	[ -f "$filename" ] || continue
	echo "--- import $filename ---";
	mongoimport -d $MONGODB_DATABASE --type csv --file $filename --headerline;
done